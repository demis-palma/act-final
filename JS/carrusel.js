
document.addEventListener("DOMContentLoaded", init );

function init() {
    var butAnterior = document.getElementById('anterior');
    var butSiguiente = document.getElementById('siguiente');

    butAnterior.addEventListener("click", atras);
    butSiguiente.addEventListener("click", adelante);
}

function atras(evento) {
    evento.preventDefault();

    var carrusel = document.querySelector('.carrusel');
    var leftMargin = parseInt(carrusel.style.marginLeft);

    var newPosition = leftMargin + 600;

    if (newPosition >= 600)
    {
        newPosition = -1800;
    }

    carrusel.style.marginLeft = newPosition + 'px';
}

function adelante(evento) {
    evento.preventDefault();

    var carrusel = document.querySelector('.carrusel');
    var leftMargin = parseInt(carrusel.style.marginLeft);

    var newPosition = leftMargin - 600;
    if (newPosition <= -2400)
    {
        newPosition = 0;
    }

    carrusel.style.marginLeft = newPosition + 'px';
}